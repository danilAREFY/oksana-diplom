package org.oksana.diplom.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.JOINED)
public class Model implements Serializable {

    @Id
    @GeneratedValue
    protected Long id;

    @CreationTimestamp
    protected Date createdDate;

    @Version
    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Integer getVersion() {
        return version;
    }
}
