package org.oksana.diplom.model.security.identity;

import org.picketlink.idm.credential.storage.TokenCredentialStorage;
import org.picketlink.idm.jpa.annotations.CredentialProperty;
import org.picketlink.idm.jpa.annotations.entity.ManagedCredential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@ManagedCredential(TokenCredentialStorage.class)
@Entity
@Table(name = "token_credential_type_entity")
public class TokenCredentialTypeEntity extends AbstractCredentialTypeEntity {

    @CredentialProperty
    @Column
    private String type;

    @CredentialProperty
    @Column(columnDefinition = "TEXT")
    private String token;

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
