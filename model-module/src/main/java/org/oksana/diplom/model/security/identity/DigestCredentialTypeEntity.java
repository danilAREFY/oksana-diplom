package org.oksana.diplom.model.security.identity;

import org.picketlink.idm.credential.storage.DigestCredentialStorage;
import org.picketlink.idm.jpa.annotations.CredentialProperty;
import org.picketlink.idm.jpa.annotations.entity.ManagedCredential;

import javax.persistence.Entity;
import javax.persistence.Table;

@ManagedCredential(DigestCredentialStorage.class)
@Entity
@Table(name = "digest_credential_type_entity")
public class DigestCredentialTypeEntity extends AbstractCredentialTypeEntity {

    @CredentialProperty(name = "realm")
    private String digestRealm;

    @CredentialProperty (name = "ha1")
    private byte[] digestHa1;

    public String getDigestRealm() {
        return digestRealm;
    }

    public void setDigestRealm(String digestRealm) {
        this.digestRealm = digestRealm;
    }

    public byte[] getDigestHa1() {
        return digestHa1;
    }

    public void setDigestHa1(byte[] digestHa1) {
        this.digestHa1 = digestHa1;
    }

}
