package org.oksana.diplom.model.security.identity;

import org.picketlink.idm.credential.storage.EncodedPasswordStorage;
import org.picketlink.idm.jpa.annotations.CredentialProperty;
import org.picketlink.idm.jpa.annotations.entity.ManagedCredential;

import javax.persistence.Entity;

@ManagedCredential(EncodedPasswordStorage.class)
@Entity
public class PasswordCredentialTypeEntity extends AbstractCredentialTypeEntity {

    @CredentialProperty(name = "encodedHash")
    private String passwordEncodedHash;

    @CredentialProperty(name = "salt")
    private String passwordSalt;

    public String getPasswordEncodedHash() {
        return passwordEncodedHash;
    }

    public void setPasswordEncodedHash(String passwordEncodedHash) {
        this.passwordEncodedHash = passwordEncodedHash;
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

}
