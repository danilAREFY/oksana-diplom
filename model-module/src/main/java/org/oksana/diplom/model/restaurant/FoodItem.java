package org.oksana.diplom.model.restaurant;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.oksana.diplom.model.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "food_item_id"))
})
@NamedQueries({
        @NamedQuery(name = "FoodItem.getAll", query = "SELECT foodItem FROM FoodItem foodItem"),
        @NamedQuery(name = "FoodItem.getMaxPrice", query = "SELECT max (foodItem.price) FROM FoodItem foodItem"),
        @NamedQuery(name = "FoodItem.getMinPrice", query = "SELECT min (foodItem.price) FROM FoodItem foodItem"),
        @NamedQuery(name = "FoodItem.searchByCriteria",
                query = "SELECT foodItem " +
                        "FROM FoodItem foodItem " +
                        "WHERE foodItem.type.id = :foodTypeId AND foodItem.price BETWEEN :minPrice AND :maxPrice")
})
@Table(name = "food_items")
public class FoodItem extends Model {

    @NotEmpty
    @Length(max = 80)
    @Column(name = "name", length = 80, nullable = false, unique = true)
    private String name;

    @Lob
    @Column(name = "description")
    private String description;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type", nullable = false)
    private FoodType type;

    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FoodType getType() {
        return type;
    }

    public void setType(FoodType foodType) {
        this.type = foodType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FoodItem foodItem = (FoodItem) o;

        return name.equals(foodItem.name);

    }

    @Override
    public int hashCode() {
        return Objects.hash(name, createdDate, description, price);
    }
}
