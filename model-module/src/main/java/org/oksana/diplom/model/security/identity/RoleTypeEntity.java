package org.oksana.diplom.model.security.identity;

import org.picketlink.idm.jpa.annotations.AttributeValue;
import org.picketlink.idm.jpa.annotations.entity.IdentityManaged;
import org.picketlink.idm.model.basic.Role;

import javax.persistence.Entity;
import javax.persistence.Table;

@IdentityManaged(Role.class)
@Entity
@Table(name = "role_type_entity")
public class RoleTypeEntity extends IdentityTypeEntity {

    @AttributeValue
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
