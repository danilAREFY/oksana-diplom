package org.oksana.diplom.model.security.identity;

import org.picketlink.idm.credential.storage.X509CertificateStorage;
import org.picketlink.idm.jpa.annotations.CredentialProperty;
import org.picketlink.idm.jpa.annotations.entity.ManagedCredential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@ManagedCredential(X509CertificateStorage.class)
@Entity
@Table(name = "x509_credential_type_entity")
public class X509CredentialTypeEntity extends AbstractCredentialTypeEntity {

    @CredentialProperty
    @Column(length = 1024)
    private String base64Cert;

    public String getBase64Cert() {
        return base64Cert;
    }

    public void setBase64Cert(String base64Cert) {
        this.base64Cert = base64Cert;
    }
}
