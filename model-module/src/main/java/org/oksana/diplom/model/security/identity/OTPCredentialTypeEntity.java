package org.oksana.diplom.model.security.identity;

import org.picketlink.idm.credential.storage.OTPCredentialStorage;
import org.picketlink.idm.jpa.annotations.CredentialProperty;
import org.picketlink.idm.jpa.annotations.entity.ManagedCredential;

import javax.persistence.Entity;
import javax.persistence.Table;

@ManagedCredential(OTPCredentialStorage.class)
@Entity
@Table(name = "otp_credential_type_entity")
public class OTPCredentialTypeEntity extends AbstractCredentialTypeEntity {

    @CredentialProperty(name = "secretKey")
    private String totpSecretKey;

    @CredentialProperty(name = "device")
    private String totpDevice;

    public String getTotpSecretKey() {
        return totpSecretKey;
    }

    public void setTotpSecretKey(String totpSecretKey) {
        this.totpSecretKey = totpSecretKey;
    }

    public String getTotpDevice() {
        return totpDevice;
    }

    public void setTotpDevice(String totpDevice) {
        this.totpDevice = totpDevice;
    }
}
