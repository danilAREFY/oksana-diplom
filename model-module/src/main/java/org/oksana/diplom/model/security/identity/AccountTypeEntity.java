package org.oksana.diplom.model.security.identity;

import org.oksana.diplom.model.security.identity.agent.SystemUser;
import org.picketlink.idm.jpa.annotations.AttributeValue;
import org.picketlink.idm.jpa.annotations.entity.IdentityManaged;
import org.picketlink.idm.model.basic.Agent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@IdentityManaged({SystemUser.class, Agent.class})
@Entity
@Table(name = "account_type_entity")
public class AccountTypeEntity extends IdentityTypeEntity {

    @AttributeValue(name = "loginName")
    @Column(name = "login", nullable = false, unique = true)
    private String login;

    @AttributeValue
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @AttributeValue
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @AttributeValue
    @Column(name = "email")
    private String email;

    public String getLogin() {
        return login;
    }

    public void setLogin(String loginName) {
        this.login = loginName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}