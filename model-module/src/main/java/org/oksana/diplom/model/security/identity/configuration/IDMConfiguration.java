package org.oksana.diplom.model.security.identity.configuration;

import org.oksana.diplom.model.security.identity.*;
import org.oksana.diplom.model.security.identity.agent.SystemUser;
import org.picketlink.annotations.PicketLink;
import org.picketlink.idm.config.IdentityConfiguration;
import org.picketlink.idm.config.IdentityConfigurationBuilder;
import org.picketlink.internal.EEJPAContextInitializer;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@SuppressWarnings("unused")
public class IDMConfiguration {

    @Inject
    private EEJPAContextInitializer contextInitializer;

    @Produces
    @PersistenceContext(unitName = "DevelopmentUnit")
    private EntityManager entityManager;

    @Produces
    @PicketLink
    private EntityManager producePicketLinkEntityManager() {
        return entityManager;
    }

    @SuppressWarnings("unchecked")
    @Produces
    private IdentityConfiguration produceIdentityConfiguration() {
        IdentityConfigurationBuilder builder = new IdentityConfigurationBuilder();
        builder
                .named("default")
                .stores()
                .jpa()
                .mappedEntity(
                        AccountTypeEntity.class,
                        AttributeTypeEntity.class,
                        GroupTypeEntity.class,
                        IdentityTypeEntity.class,
                        PartitionTypeEntity.class,
                        PasswordCredentialTypeEntity.class,
                        RelationshipIdentityTypeEntity.class,
                        RoleTypeEntity.class,
                        RelationshipTypeEntity.class)
                .addContextInitializer(contextInitializer)
                .supportType(SystemUser.class)
                .supportAllFeatures();

        return builder.build();
    }

}
