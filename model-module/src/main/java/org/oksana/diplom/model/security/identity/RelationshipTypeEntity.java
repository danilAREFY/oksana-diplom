package org.oksana.diplom.model.security.identity;

import org.picketlink.idm.jpa.annotations.RelationshipClass;
import org.picketlink.idm.jpa.annotations.entity.IdentityManaged;
import org.picketlink.idm.model.Relationship;

import javax.persistence.Entity;

@IdentityManaged(Relationship.class)
@Entity
public class RelationshipTypeEntity extends AttributedTypeEntity {

    @RelationshipClass
    private String typeName;

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

}
