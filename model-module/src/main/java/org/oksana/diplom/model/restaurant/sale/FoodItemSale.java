package org.oksana.diplom.model.restaurant.sale;

import org.oksana.diplom.model.restaurant.FoodItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "food_item_sale_id"))
})
@NamedQueries({
        @NamedQuery(name = "FoodItemSale.findByFoodItemId",
                query = "SELECT foodItemSale FROM FoodItemSale foodItemSale WHERE foodItemSale.foodItem.id = :foodItemId")
})
@Table(name = "food_item_sales")
public class FoodItemSale extends Sale {

    @NotNull
    @OneToOne
    @JoinColumn(name = "food_item_fk", nullable = false)
    private FoodItem foodItem;

    public FoodItem getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(FoodItem foodItem) {
        this.foodItem = foodItem;
    }
}
