package org.oksana.diplom.model.security.identity.agent;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.picketlink.idm.model.annotation.AttributeProperty;
import org.picketlink.idm.model.annotation.IdentityStereotype;
import org.picketlink.idm.model.basic.Agent;
import org.picketlink.idm.query.QueryParameter;

import static org.picketlink.idm.model.annotation.IdentityStereotype.Stereotype.USER;

@IdentityStereotype(USER)
public class SystemUser extends Agent {

    public static final QueryParameter FIRST_NAME   = QUERY_ATTRIBUTE.byName("firstName");
    public static final QueryParameter LAST_NAME    = QUERY_ATTRIBUTE.byName("lastName");
    public static final QueryParameter EMAIL        = QUERY_ATTRIBUTE.byName("email");

    @NotEmpty
    @AttributeProperty
    private String firstName;

    @NotEmpty
    @AttributeProperty
    private String lastName;

    @Email
    @AttributeProperty
    private String email;

    public SystemUser() {
    }

    public SystemUser(String loginName) {
        super(loginName);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
