package org.oksana.diplom.model.restaurant;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.oksana.diplom.model.Model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "food_type_id"))
})
@NamedQueries({
        @NamedQuery(name = "FoodType.getAll", query = "SELECT foodType FROM FoodType foodType"),
        @NamedQuery(name = "FoodType.getByDate", query =
                "SELECT count(foodType) FROM FoodType foodType WHERE foodType.createdDate = :createdDate")
})
@Table(name = "food_types")
public class FoodType extends Model {

    @NotEmpty
    @Length(min = 3, max = 50)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Lob
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "type", fetch = FetchType.EAGER)
    private List<FoodItem> foodItems;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FoodItem> getFoodItems() {
        return foodItems;
    }

    public void setFoodItems(List<FoodItem> foodItems) {
        this.foodItems = foodItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FoodType foodType = (FoodType) o;

        return name.equals(foodType.name);

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createdDate, name);
    }

}
