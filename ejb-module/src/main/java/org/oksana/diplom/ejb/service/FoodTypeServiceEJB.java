package org.oksana.diplom.ejb.service;

import org.oksana.diplom.model.restaurant.FoodType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class FoodTypeServiceEJB implements EntityServiceEJB<FoodType> {

    @Inject
    private Logger logger;

    @Inject
    private EntityManager entityManager;

    @Override
    public List<FoodType> getAll() {
        TypedQuery<FoodType> namedQuery = entityManager.createNamedQuery("FoodType.getAll", FoodType.class);
        return namedQuery.getResultList();
    }

}
