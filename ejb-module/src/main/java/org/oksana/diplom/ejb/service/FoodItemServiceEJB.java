package org.oksana.diplom.ejb.service;

import org.oksana.diplom.model.restaurant.FoodItem;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class FoodItemServiceEJB implements EntityServiceEJB<FoodItem> {

    @Inject
    private Logger logger;

    @Inject
    private EntityManager entityManager;

    @Override
    public List<FoodItem> getAll() {
        TypedQuery<FoodItem> namedQuery = entityManager.createNamedQuery("FoodItem.getAll", FoodItem.class);
        return namedQuery.getResultList();
    }
}
