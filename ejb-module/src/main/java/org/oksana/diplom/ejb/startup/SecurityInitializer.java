package org.oksana.diplom.ejb.startup;

import org.picketlink.idm.PartitionManager;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Singleton
@Startup
public class SecurityInitializer {

    @Inject
    private PartitionManager partitionManager;

    @PostConstruct
    private void init() {

//        User user = new User();
//
//        user.setLoginName("admin");
//        user.setEmail("admin@gmail.com");
//        user.setFirstName("admin");
//        user.setLastName("admin");
//
//        IdentityManager identityManager = partitionManager.createIdentityManager();
//        identityManager.add(user);
//        identityManager.updateCredential(user, new Password("admin"));
    }

}
