package org.oksana.diplom.ejb.service;

import java.util.List;

public interface EntityServiceEJB<T> {

    List<T> getAll();

}
