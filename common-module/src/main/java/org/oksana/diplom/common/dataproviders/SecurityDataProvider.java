package org.oksana.diplom.common.dataproviders;

import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;

import java.util.List;

public interface SecurityDataProvider {

    List<Group> getGroups();
    Group getGroup(String groupName);
    List<Group> getUserGroups(String userName);

    List<Role> getRoles();
    Role getRole(String roleName);
    List<Role> getUserRoles(String userName);

}
