package org.oksana.diplom.workplace.food.converter;

import org.oksana.diplom.model.restaurant.FoodType;

import javax.enterprise.inject.Model;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Model
@FacesConverter("foodTypeConverter")
public class FoodTypeConverter implements Converter {

    @Inject
    private EntityManager entityManager;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        if (value != null && value.trim().length() > 0) {
            return entityManager.find(FoodType.class, Long.valueOf(value));
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if (value != null) {
            return String.valueOf(((FoodType) value).getId());
        }

        return null;
    }
}
