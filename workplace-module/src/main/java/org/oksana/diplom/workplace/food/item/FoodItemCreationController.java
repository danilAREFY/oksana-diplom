package org.oksana.diplom.workplace.food.item;

import org.oksana.diplom.ejb.service.FoodItemServiceEJB;
import org.oksana.diplom.model.restaurant.FoodItem;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@RequestScoped
public class FoodItemCreationController implements Serializable {

    @Inject
    private Logger logger;

    @Inject
    private EntityManager entityManager;

    @Inject
    private UserTransaction userTransaction;

    @Inject
    private FacesContext facesContext;

    @Inject
    private FoodItem foodItem;

    @Inject
    private FoodItemServiceEJB foodItemServiceEJB;

    public String create() {
        try {
            userTransaction.begin();

            for (FoodItem item : foodItemServiceEJB.getAll()) {
                if (Objects.equals(item.getName(), foodItem.getName())) {
                    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Food item with name \" " + foodItem.getName() + " \" already exist.",
                            "Food item with name \" " + foodItem.getName() + " \" already exist."));
                    return null;
                }
            }

            entityManager.persist(foodItem);
            userTransaction.commit();

        } catch (Exception e) {
            logger.log(Level.SEVERE, "an exception was thrown: ", e);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Cannot create item. Try again later.",
                    "Cannot create item. Try again later."));
        } finally {

            try {
                if (userTransaction.getStatus() == Status.STATUS_ACTIVE) {
                    userTransaction.rollback();
                }
            } catch (SystemException e) {
                logger.log(Level.SEVERE, "an exception was thrown: ", e);
            }
        }
        return null;
    }

    public FoodItem getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(FoodItem foodItem) {
        this.foodItem = foodItem;
    }

}
