package org.oksana.diplom.workplace.food.diagram;

import org.oksana.diplom.model.restaurant.FoodItem;
import org.oksana.diplom.model.restaurant.FoodType;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

@Named
@ViewScoped
public class FoodItemsDiagramController implements Serializable {

    @Inject
    private Logger logger;

    private FoodType selectedFoodType;
    private DefaultDiagramModel model;

    public void foodTypeSelectionChanged(final AjaxBehaviorEvent event) {
        selectedFoodType = (FoodType) ((SelectOneMenu) event.getSource()).getValue();

        model = new DefaultDiagramModel();
        if (selectedFoodType == null) {
            return;
        }

        model.setMaxConnections(-1);
        Element root = new Element(selectedFoodType.getName(), "20em", "6em");
        root.addEndPoint(new DotEndPoint(EndPointAnchor.BOTTOM));
        model.addElement(root);

        List<FoodItem> foodItems = selectedFoodType.getFoodItems();
        for (FoodItem foodItem : foodItems) {
            Element element = new Element(foodItem.getName(), "20em", "6em");
            element.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));

            model.addElement(element);
        }
        for (Element element : model.getElements()) {
            model.connect(new Connection(root.getEndPoints().get(0), element.getEndPoints().get(0)));
        }

    }

    public FoodType getSelectedFoodType() {
        return selectedFoodType;
    }

    public void setSelectedFoodType(FoodType selectedFoodType) {
        this.selectedFoodType = selectedFoodType;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }
}
