package org.oksana.diplom.workplace.food.item.cart;

import org.oksana.diplom.model.restaurant.FoodItem;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Map;

@Named
@ViewScoped
public class CartAddController implements Serializable {

    @Inject
    private CartController cartController;

    @Inject
    private FacesContext facesContext;

    private FoodItem selectedFoodItem;
    private int count = 1;

    public void add() {

        Map<FoodItem, Integer> container = cartController.getContainer();
        Integer integer = container.get(selectedFoodItem);
        if (integer != null) {
            integer += count;
            container.put(selectedFoodItem, integer);
        } else {
            container.put(selectedFoodItem, count);
        }
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Item was successfully added to cart",
                "Item was successfully added to cart"));

    }

    public FoodItem getSelectedFoodItem() {
        return selectedFoodItem;
    }

    public void setSelectedFoodItem(FoodItem selectedFoodItem) {
        this.selectedFoodItem = selectedFoodItem;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
