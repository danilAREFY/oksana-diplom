package org.oksana.diplom.workplace.food.type;

import org.oksana.diplom.ejb.service.FoodTypeServiceEJB;
import org.oksana.diplom.model.restaurant.FoodType;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Named
@RequestScoped
public class FoodTypes {

    @Inject
    private EntityManager entityManager;

    @Inject
    private FoodTypeServiceEJB foodTypeService;

    @Named(value = "foodTypesList")
    @Produces
    private List<FoodType> produceFoodTypeList() {
        return foodTypeService.getAll();
    }

    @Transactional
    public void removeFoodType(Long foodTypeId) {
        FoodType foodType = entityManager.find(FoodType.class, foodTypeId);
        entityManager.remove(foodType);
    }

}
