package org.oksana.diplom.workplace.food.item;

import org.oksana.diplom.model.restaurant.FoodItem;
import org.oksana.diplom.model.restaurant.FoodType;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

@Named
@ViewScoped
public class FoodItemSearchController implements Serializable {

    @Inject
    private Logger logger;

    @Inject
    private EntityManager entityManager;

    private FoodType selectedFoodType;
    private double min;
    private double max;

    private List<FoodItem> foundedFoodItems;

    @PostConstruct
    private void init() {
        TypedQuery<Double> namedQuery = entityManager.createNamedQuery("FoodItem.getMinPrice", Double.class);

        //TODO WTF with NPE??
        try {
            min = namedQuery.getSingleResult();

            namedQuery = entityManager.createNamedQuery("FoodItem.getMaxPrice", Double.class);
            max = namedQuery.getSingleResult();
        } catch (NullPointerException ignored) {
        }
    }

    public void search() {

        if (selectedFoodType == null) {
            return;
        }

        TypedQuery<FoodItem> namedQuery = entityManager.createNamedQuery("FoodItem.searchByCriteria", FoodItem.class);
        namedQuery.setParameter("foodTypeId", selectedFoodType.getId());
        namedQuery.setParameter("minPrice", min);
        namedQuery.setParameter("maxPrice", max);

        foundedFoodItems = namedQuery.getResultList();
    }

    public void foodTypeSelectionChanged(final AjaxBehaviorEvent event) {
        selectedFoodType = (FoodType) ((SelectOneMenu) event.getSource()).getValue();
    }

    public FoodType getSelectedFoodType() {
        return selectedFoodType;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public List<FoodItem> getFoundedFoodItems() {
        return foundedFoodItems;
    }

    public void setFoundedFoodItems(List<FoodItem> foundedFoodItems) {
        this.foundedFoodItems = foundedFoodItems;
    }
}
