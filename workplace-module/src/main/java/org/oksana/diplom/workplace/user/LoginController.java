package org.oksana.diplom.workplace.user;

import org.picketlink.Identity;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.logging.Logger;

@Named
@RequestScoped
public class LoginController implements Serializable {

    @Inject
    private Logger logger;

    @SuppressWarnings("CdiInjectionPointsInspection")
    @Inject
    private Identity identity;

    @Inject
    private FacesContext facesContext;

    public String login() {
        Identity.AuthenticationResult result = identity.login();

        if (Identity.AuthenticationResult.FAILED.equals(result)) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Authentication was unsuccessful.", "Please check your username and password before trying again."));
            return null;
        }
        return "admin/index.xhtml?faces-redirect=true";
    }

}
