package org.oksana.diplom.workplace.converter;

import org.oksana.diplom.workplace.user.PreferencesController;

import javax.enterprise.inject.Model;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

@Model
@FacesConverter("localeConverter")
public class LocaleConverter implements Converter {

    @Inject
    private PreferencesController preferencesController;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return preferencesController.getLocales().get(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null) {
            for (Map.Entry<String, Locale> stringLocaleEntry : preferencesController.getLocales().entrySet()) {
                if (Objects.equals(value, stringLocaleEntry.getValue())) {
                    return stringLocaleEntry.getKey();
                }
            }
        }
        return null;
    }
}
