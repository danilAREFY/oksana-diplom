package org.oksana.diplom.workplace.food.item;

import org.oksana.diplom.ejb.service.FoodItemServiceEJB;
import org.oksana.diplom.model.restaurant.FoodItem;

import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;

@Model
public class FoodItems {

    @Inject
    private Logger logger;

    @Inject
    private EntityManager entityManager;

    @Inject
    private FoodItemServiceEJB foodItemServiceEJB;

    @Named("foodItemsList")
    @Produces
    private List<FoodItem> produceFoodItemList() {
        return foodItemServiceEJB.getAll();
    }

    @Transactional
    public void removeFoodItem(Long foodItemId) {
        FoodItem foodItem = entityManager.find(FoodItem.class, foodItemId);
        entityManager.remove(foodItem);
    }

}
