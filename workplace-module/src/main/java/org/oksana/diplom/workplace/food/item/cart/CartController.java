package org.oksana.diplom.workplace.food.item.cart;

import org.oksana.diplom.model.restaurant.FoodItem;
import org.oksana.diplom.model.restaurant.sale.FoodItemSale;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@SessionScoped
public class CartController implements Serializable {

    @Inject
    private EntityManager entityManager;

    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    @Inject
    private transient FacesContext facesContext;

    private Map<FoodItem, Integer> container;

    @PostConstruct
    private void init() {
        container = new HashMap<>();
    }

    @Transactional
    public void checkOut() {
        for (Map.Entry<FoodItem, Integer> containerEntry : container.entrySet()) {
            TypedQuery<FoodItemSale> namedQuery = entityManager.createNamedQuery("FoodItemSale.findByFoodItemId", FoodItemSale.class);
            namedQuery.setParameter("foodItemId", containerEntry.getKey().getId());

            List<FoodItemSale> resultList = namedQuery.getResultList();
            if (resultList.isEmpty()) {
                FoodItemSale foodItemSale = new FoodItemSale();
                foodItemSale.setFoodItem(containerEntry.getKey());
                foodItemSale.setCount(containerEntry.getValue());
                entityManager.persist(foodItemSale);
            } else {
                //unique constraint
                FoodItemSale foodItemSale = resultList.get(0);
                foodItemSale.setCount(foodItemSale.getCount() + containerEntry.getValue());
                entityManager.merge(foodItemSale);
            }

        }

        container.clear();
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Congratulations, foods were purchased!",
                "Congratulations, foods were purchased!"));
    }

    public Map<FoodItem, Integer> getContainer() {
        return container;
    }

    public void setContainer(Map<FoodItem, Integer> container) {
        this.container = container;
    }
}
