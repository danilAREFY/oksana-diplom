package org.oksana.diplom.workplace.food.item;

import org.oksana.diplom.model.restaurant.FoodItem;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class FoodItemController implements Serializable {

    private FoodItem selectedFoodItem;

    public FoodItem getSelectedFoodItem() {
        return selectedFoodItem;
    }

    public void setSelectedFoodItem(FoodItem selectedFoodItem) {
        this.selectedFoodItem = selectedFoodItem;
    }
}
