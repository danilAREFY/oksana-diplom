package org.oksana.diplom.workplace.user;

import org.hibernate.validator.constraints.NotEmpty;
import org.oksana.diplom.model.security.identity.agent.SystemUser;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.Agent;
import org.picketlink.idm.query.IdentityQuery;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

@Named
@RequestScoped
public class RegistrationController {

    @SuppressWarnings("CdiInjectionPointsInspection")
    @Inject
    private IdentityManager identityManager;

    @Inject
    private SystemUser user;

    @Inject
    private FacesContext facesContext;

    @NotEmpty
    private String password;

    @Transactional
    public String register() {

        IdentityQuery<SystemUser> identityQuery = identityManager.createIdentityQuery(SystemUser.class);
        identityQuery.setParameter(Agent.LOGIN_NAME, user.getLoginName());

        if (identityQuery.getResultList().size() != 0) {
            SystemUser foundedUser = identityQuery.getResultList().get(0);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Account with name \" " + foundedUser.getLoginName() + " \" was successfully created.",
                    "Account with name \" " + foundedUser.getLoginName() + " \" was successfully created."));
            return null;
        }

        Password password = new Password(this.password);
        identityManager.add(user);
        identityManager.updateCredential(user, password);

        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Account with name \" " + user.getLoginName() + " \" was successfully created.",
                "Account with name \" " + user.getLoginName() + " \" was successfully created."));
        return null;
    }

    public SystemUser getUser() {
        return user;
    }

    public void setUser(SystemUser user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
