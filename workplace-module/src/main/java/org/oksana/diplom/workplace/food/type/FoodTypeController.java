package org.oksana.diplom.workplace.food.type;

import org.oksana.diplom.model.restaurant.FoodType;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class FoodTypeController implements Serializable {

    private FoodType selectedFoodType;

    public FoodType getSelectedFoodType() {
        return selectedFoodType;
    }

    public void setSelectedFoodType(FoodType selectedFoodType) {
        this.selectedFoodType = selectedFoodType;
    }

}
