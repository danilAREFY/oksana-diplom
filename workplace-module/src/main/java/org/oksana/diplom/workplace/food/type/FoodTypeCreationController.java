package org.oksana.diplom.workplace.food.type;

import org.oksana.diplom.ejb.service.FoodTypeServiceEJB;
import org.oksana.diplom.model.restaurant.FoodType;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;

@Named
@RequestScoped
public class FoodTypeCreationController implements Serializable {

    @Inject
    private Logger logger;

    @Inject
    private EntityManager entityManager;

    @Inject
    private FacesContext facesContext;

    @Inject
    private FoodType foodType;

    @Inject
    private FoodTypeServiceEJB foodTypeServiceEJB;

    @Transactional
    public String create() {

        for (FoodType type : foodTypeServiceEJB.getAll()) {
            if (Objects.equals(foodType.getName(), type.getName())) {
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Food type with name \" " + foodType.getName() + " \" already exists!",
                        "Food type with name \" " + foodType.getName() + " \" already exists!"));
                return null;
            }
        }

        entityManager.persist(foodType);
        return null;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

}
