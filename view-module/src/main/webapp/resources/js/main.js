var restaurant = (function() {

    var utils = (function () {
        var redirect = function (url) {
            window.location.href = url;
        };

        var waitAndRedirect = function (url, timeout) {
            setTimeout(function() {
                window.location.href = url;
            }, timeout);
        };


        return {
            redirect : redirect,
            waitAndRedirect : waitAndRedirect
        };

    }());

    return {
        utils: utils
    };

}());