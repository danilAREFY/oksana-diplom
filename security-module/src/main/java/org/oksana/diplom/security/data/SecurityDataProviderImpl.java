package org.oksana.diplom.security.data;

import org.oksana.diplom.common.dataproviders.SecurityDataProvider;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;

import java.util.List;

public class SecurityDataProviderImpl implements SecurityDataProvider {

    private IdentityManager identityManager;

    public List<Group> getGroups() {
        return identityManager.createIdentityQuery(Group.class).getResultList();
    }

    public Group getGroup(String groupName) {
        return null;
    }

    public List<Group> getUserGroups(String userName) {
        return null;
    }

    public List<Role> getRoles() {
        return null;
    }

    public Role getRole(String roleName) {
        return null;
    }

    public List<Role> getUserRoles(String userName) {
        return null;
    }
}
