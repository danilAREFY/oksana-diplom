package org.oksana.diplom.security.filter;

import org.picketlink.Identity;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/admin/*")
public class RealmProtectionFilter implements Filter {

    @Inject
    private Instance<Identity> identityInstance;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        if (identityInstance.get().isLoggedIn()) {
            chain.doFilter(httpRequest, httpResponse);
        } else {
            forwardAccessDeniedPage(httpRequest, httpResponse);
        }
    }

    private void forwardAccessDeniedPage(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws ServletException, IOException {
        httpRequest.getServletContext().getRequestDispatcher("/login.xhtml").forward(httpRequest, httpResponse);
    }

    @Override
    public void destroy() {
    }
}
